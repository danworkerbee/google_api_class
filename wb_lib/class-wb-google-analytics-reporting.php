<?php
/**
 * Google Analytic API V4 Simplified Class For Wordpress
 *
 * @link       http://workerbee.tv/
 * @since      1.0.0
 *
 * @package    wb_ga_reporting
 * @subpackage wb_ga_reporting/vendor
 * @package    wb_ga_reporting
 * @author     Dan Ambas
 *
 * Resources & API Documentation
 * @link https://developers.google.com/analytics/devguides/reporting/core/v4/quickstart/service-php
 * @link https://ga-dev-tools.appspot.com/query-explorer/
 * @link https://developers.google.com/analytics/devguides/reporting/core/v4/sample
 * @link https://developers.google.com/analytics/devguides/reporting/core/v4/rest/v4/reports/batchGet#Operator
*/

if (!defined('ABSPATH')) die();

if (!class_exists('wb_ga_reporting'))
{

    class wb_ga_reporting
    {

        public $args;

        protected $ga_view_id;
        protected $ga_app_name;
        protected $ga_key_file_path;
        protected $ga_metrics;
        protected $ga_dimensions;

        protected $ga_query;
        protected $ga_date_ranges;
        protected $ga_query_dimensions;
        protected $ga_query_metrics;
        protected $ga_query_segments;

        function __construct($args = '')
        {
            $this->args = array_merge($this->default_option() , $args);
            $this->ga_key_file_path = $this->args['ga_key_file_path'];
            $this->ga_app_name = $this->args['ga_app_name'];
            $this->ga_view_id = $this->args['ga_view_id'];
            $this->ga_metrics = $this->args['ga_metrics'];
            $this->ga_dimensions = $this->args['ga_dimensions'];

            $this->ga_query = $this->args['ga_query'];
            $this->ga_query_date_ranges = $this->ga_query['date_ranges'];
            $this->ga_query_dimensions = $this->ga_query['dimensions'];
            $this->ga_query_metrics = $this->ga_query['metrics'];
            $this->ga_query_segments = $this->ga_query['segments'];

            $this->includes();
        }

        private function includes()
        {
           if (!defined('PLUGIN_DIR')) define('PLUGIN_DIR', plugin_dir_path(__FILE__));
           if (!class_exists('Google_Client')) require_once PLUGIN_DIR . '/vendor/autoload.php';
        }

        public function data()
        {
            if ($this->error()) return $this->error();
            try
            {
                $request = new Google_Service_AnalyticsReporting_ReportRequest();
                $request->setViewId($this->ga_view_id);
                $request->setDateRanges($this->query_date_range());
                $request->setDimensions($this->set_dimensions());
                $request->setMetrics($this->set_metrics());
                if ($this->query_dimensions())
                {
                    $request->setDimensionFilterClauses(array(
                        $this->query_dimensions()
                    ));
                }
                if ($this->query_metrics())
                {
                    $request->setMetricFilterClauses(array(
                        $this->query_metrics()
                    ));
                }
                if ( $this->query_segments() )
                {
                    $request->setSegments($this->query_segments());
                }
                $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
                $body->setReportRequests(array(
                    $request
                ));
                $reports = $this->analytics()
                    ->reports
                    ->batchGet($body);

                return $reports;
            }
            catch(\Exception $e)
            {
                return 'There was an Analytics API service error ' . $e->getCode() . ':' . $e->getMessage();
            }
        }

        private function default_option()
        {
            return array(
                'ga_key_file_path' => '',
                'ga_app_name' => 'All Web Site Data',
                'ga_view_id' => '',
                'ga_dimensions' => array(
                    'ga:hostname',
                    'ga:pagePath',
                    'ga:pageTitle'
                ) ,
                'ga_metrics' => array(
                    'ga:pageviews',
                    'ga:uniquePageviews',
                    'ga:avgTimeOnPage',
                    'ga:entrances'
                ) ,
                'ga_query' => array(
                    'date_ranges' => array(
                        array(
                            'setStartDate' => '7daysAgo',
                            'setEndDate' => 'today'
                        )
                    ) ,
                    'dimensions' => array(
                        array(
                            'dimension' => '',
                            'operator' => '',
                            'expression' => ''
                        )
                    ) ,
                    'metrics' => array(
                        array(
                            'metric' => '',
                            'operator' => '',
                            'expression' => ''
                        )
                    )
                )
            );
        }

        private function error()
        {
            if (!$this->ga_view_id) $message = 'View ID Not Define';
            if (!file_exists($this->ga_key_file_path)) $message = 'Key File Not Found';

            return $message;
        }

        private function analytics()
        {
            $client = new Google_Client();
            $client->setApplicationName($this->ga_app_name);
            $client->setAuthConfig($this->ga_key_file_path);
            $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
            $analytics = new Google_Service_AnalyticsReporting($client);
            return $analytics;
        }

        private function set_metrics()
        {
            foreach ($this->ga_metrics as $metric)
            {
                $_metric = new Google_Service_AnalyticsReporting_Metric();
                $_metric->setExpression($metric);
                $metrics[] = $_metric;
            }
            return $metrics;
        }

        private function set_dimensions()
        {
            foreach ($this->ga_dimensions as $dimension)
            {
                $_dimension = new Google_Service_AnalyticsReporting_Dimension();
                $_dimension->setName($dimension);
                $dimensions[] = $_dimension;
            }

            if ( array_filter($this->query_segments()) ) {
	            $segmentDimensions = new Google_Service_AnalyticsReporting_Dimension();
	            $segmentDimensions->setName("ga:segment");
	            $dimensions[] = $segmentDimensions;
        	}

            return $dimensions;
        }

        private function query_date_range()
        {
            foreach ($this->ga_query_date_ranges as $date)
            {
                $dateRange = new Google_Service_AnalyticsReporting_DateRange();
                $dateRange->setStartDate($date['setStartDate']);
                $dateRange->setEndDate($date['setEndDate']);
                $dateRanges[] = $dateRange;
            }
            return $dateRanges;
        }

        private function query_dimensions()
        {
            foreach ($this->ga_query_dimensions as $query_dimension)
            {
                if (!is_array($query_dimension))
                {
                    $operator = $query_dimension;
                }
                else
                {
                    if (array_key_exists('dimension', $query_dimension) && array_key_exists('operator', $query_dimension) && array_key_exists('expression', $query_dimension))
                    {
                        if ($query_dimension['metric'] != '' && $query_dimension['operator'] != '' && $query_dimension['expression'] != '')
                        {
                            $dimensionFilter = new Google_Service_AnalyticsReporting_DimensionFilter();
                            $dimensionFilter->setDimensionName($query_dimension['dimension']);
                            $dimensionFilter->setOperator($query_dimension['operator']);
                            $dimensionFilter->setExpressions($query_dimension['expression']);
                            $dimensionFilters[] = $dimensionFilter;
                        }
                    }
                }
            }
            if (count($dimensionFilters) > 0)
            {
                $filter_clause = new Google_Service_AnalyticsReporting_DimensionFilterClause();
                $filter_clause->setFilters($dimensionFilters);
                if ($operator)
                {
                    $filter_clause->setOperator($operator);
                }
                return $filter_clause;
            }
        }

        private function query_metrics()
        {
            foreach ($this->ga_query_metrics as $query_metric)
            {
                if (!is_array($query_metric))
                {
                    $operator = $query_metric;
                }
                else
                {
                    if (array_key_exists('metric', $query_metric) && array_key_exists('operator', $query_metric) && array_key_exists('expression', $query_metric))
                    {
                        if ($query_metric['metric'] != '' && $query_metric['operator'] != '' && $query_metric['expression'] != '')
                        {
                            $metricFilter = new Google_Service_AnalyticsReporting_MetricFilter();
                            $metricFilter->setMetricName($query_metric['metric']);
                            $metricFilter->setOperator($query_metric['operator']);
                            $metricFilter->setComparisonValue($query_metric['expression']);
                            $metricFilters[] = $metricFilter;
                        }
                    }
                }
            }
            if (count($metricFilters) > 0)
            {
                $filter_clause = new Google_Service_AnalyticsReporting_MetricFilterClause();
                $filter_clause->setFilters($metricFilters);
                if ($operator)
                {
                    $filter_clause->setOperator($operator);
                }
                return $filter_clause;
            }
        }

        private function query_segments()
        {
            foreach ($this->ga_query_segments as $segment)
            {
                $segments[] = $this->get_segments($segment['label'], $segment['dimension'], $segment['operator'], $segment['expression']);
            }
            return $segments;
        }

        private function get_segments($segmentName, $dimension, $operator, $dimensionFilterExpression)
        {
            // Create Dimension Filter.
            $dimensionFilter = new Google_Service_AnalyticsReporting_SegmentDimensionFilter();
            $dimensionFilter->setDimensionName($dimension);
            $dimensionFilter->setOperator($operator);
            $dimensionFilter->setExpressions(array(
                $dimensionFilterExpression
            ));

            // Create Segment Filter Clause.
            $segmentFilterClause = new Google_Service_AnalyticsReporting_SegmentFilterClause();
            $segmentFilterClause->setDimensionFilter($dimensionFilter);

            // Create the Or Filters for Segment.
            $orFiltersForSegment = new Google_Service_AnalyticsReporting_OrFiltersForSegment();
            $orFiltersForSegment->setSegmentFilterClauses(array(
                $segmentFilterClause
            ));

            // Create the Simple Segment.
            $simpleSegment = new Google_Service_AnalyticsReporting_SimpleSegment();
            $simpleSegment->setOrFiltersForSegment(array(
                $orFiltersForSegment
            ));

            // Create the Segment Filters.
            $segmentFilter = new Google_Service_AnalyticsReporting_SegmentFilter();
            $segmentFilter->setSimpleSegment($simpleSegment);

            // Create the Segment Definition.
            $segmentDefinition = new Google_Service_AnalyticsReporting_SegmentDefinition();
            $segmentDefinition->setSegmentFilters(array(
                $segmentFilter
            ));

            // Create the Dynamic Segment.
            $dynamicSegment = new Google_Service_AnalyticsReporting_DynamicSegment();
            $dynamicSegment->setSessionSegment($segmentDefinition);
            $dynamicSegment->setName($segmentName);

            // Create the Segments object.
            $segment = new Google_Service_AnalyticsReporting_Segment();
            $segment->setDynamicSegment($dynamicSegment);
            return $segment;
        }
    }
} // end wb_ga_reporting class


