<?php
/**
 * Google Analytic API V4 Simplified Class For Wordpress
 *
 * @link       http://workerbee.tv/
 * @since      1.0.0
 *
 * @package    wb_ga_api
 * @subpackage wb_ga_api/includes
 * @subpackage wb_ga_api/vendor
 * @package    wb_ga_api
 * @author     Dan Ambas
 *
 * Resources & API Documentation
 * @link https://developers.google.com/analytics/devguides/config/mgmt/v3/mgmtReference
*/

if (!defined('ABSPATH')) die();

if (!class_exists('wb_ga_api')) {

    class wb_ga_api
    {      
        protected $ga_key_file_path;
        protected $ga_account_id;

        public $property;
        public $profile;
        public $custom_dimension;
        public $custom_metrics;

        function __construct($ga_account_id='', $ga_key_file_path = '')
        {
           $this->ga_key_file_path  = $ga_key_file_path;
           $this->ga_account_id     = $ga_account_id;

           if ($this->error()) return $this->error();
             
           $this->includes();
           $this->instances();
        }

        private function includes()
        {
            if (!defined('PLUGIN_DIR')) 
                define('PLUGIN_DIR', plugin_dir_path(__FILE__));

            if (!class_exists('Google_Client')) 
                require_once PLUGIN_DIR . '/vendor/autoload.php';

            require_once PLUGIN_DIR . '/includes/profile.php';
            require_once PLUGIN_DIR . '/includes/property.php';
            require_once PLUGIN_DIR . '/includes/custom_metric.php';
            require_once PLUGIN_DIR . '/includes/custom_dimension.php';
        }

        private function instances()
        {
            $this->profile           = new wb_ga_profile($this->analytics(), $this->ga_account_id);
            $this->property          = new wb_ga_property($this->analytics(), $this->ga_account_id);
            $this->custom_metrics    = new wb_ga_custom_metrics($this->analytics(), $this->ga_account_id);
            $this->custom_dimensions = new wb_ga_custom_dimension($this->analytics(),$this->ga_account_id);
        }

        private function error()
        {
            if (!$this->ga_account_id) 
                $message = 'Insert Account ID';
            if (!file_exists($this->ga_key_file_path)) 
                $message = 'Key File Not Found';
            return $message;
        }

        private function analytics() {
            // Create and configure a new client object.
            $client = new Google_Client();
            $client->setApplicationName('Web Client API');
            $client->setAuthConfig($this->ga_key_file_path);
            $client->setScopes(['https://www.googleapis.com/auth/analytics.edit','https://www.googleapis.com/auth/analytics.readonly']);
            $analytics = new Google_Service_Analytics($client);
            return $analytics;
        }
    } 
}


