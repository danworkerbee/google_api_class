<?php 

if (!class_exists('wb_ga_custom_dimension'))
{
    class wb_ga_custom_dimension
    {  
    	private $analytic;
    	private $account_id;
    	private $dimension;

    	function __construct($anayltic = '', $account_id = '')
        {	
        	$this->analytic = $anayltic;
        	$this->account_id = $account_id;
        	$this->dimension = new Google_Service_Analytics_Customdimension();
        }

        public function list($propertyID) {
        	if ( !$propertyID ) return 'Please Add Property ID';

        	try {
            	return $this->analytic->management_customDimensions->listManagementCustomdimensions($this->account_id, $propertyID);
        	} 
        	catch(\Exception $e) {
                return 'There was an Analytics API service error ' . $e->getCode() . ':' . $e->getMessage();
            }
        }

        public function get($propertyID, $dimensionID) {
        	if ( !$propertyID ) return 'Please Add Property ID';
        	if ( !$dimensionID ) return 'Please Add dimension ID';

        	try {
            	return $this->analytic->management_customDimensions->get($this->account_id, $propertyID, $dimensionID);
        	} 
        	catch(\Exception $e) {
                return 'There was an Analytics API service error ' . $e->getCode() . ':' . $e->getMessage();
            }
        }

        public function insert($propertyID, $name, $scope, $state = TRUE) {
        	if ( !$propertyID ) return 'Please Add Property ID';
        	if ( !$name ) return 'Please Add dimension Name';
        	if ( !$type ) return 'Please Add dimension Type';
        	if ( !$scope ) return 'Please Add dimension Scope';

        	try {

				$this->dimension->setName($name);
				$this->dimension->setScope($scope);
				$this->dimension->setActive($state);

            	return $this->analytic->management_customDimensions->insert($this->account_id, $propertyID, $this->dimension);
        	} 
        	catch(\Exception $e) {
                return 'There was an Analytics API service error ' . $e->getCode() . ':' . $e->getMessage();
            }
        }

        public function update($propertyID, $dimensionID , $name, $scope, $state = TRUE) {
        	if ( !$dimensionID) return 'Please Add dimension ID';
        	if ( !$propertyID ) return 'Please Add Property ID';
        	if ( !$name ) return 'Please Add dimension Name';
        	if ( !$type ) return 'Please Add dimension Type';
        	if ( !$scope ) return 'Please Add dimension Scope';

        	try {

				$this->dimension->setName($name);
				$this->dimension->setScope($scope);
				$this->dimension->setActive($state);

            	return $this->analytic->management_customDimensions->update($this->account_id, $propertyID, $dimensionID, $this->dimension);
        	} 
        	catch(\Exception $e) {
                return 'There was an Analytics API service error ' . $e->getCode() . ':' . $e->getMessage();
            }
        }
    }
}