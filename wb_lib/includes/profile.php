<?php 

if (!class_exists('wb_ga_profile'))
{
    class wb_ga_profile
    {  
    	private $analytic;
    	private $account_id;
    	private $profile;

    	function __construct($anayltic = '', $account_id = '')
        {	
        	$this->analytic = $anayltic;
        	$this->account_id = $account_id;
        	$this->profile = new Google_Service_Analytics_Profile();
        }

        public function list($propertyID) {
        	if ( !$propertyID ) return 'Please Add Property ID';

        	try {
            	return $this->analytic->management_profiles->listManagementProfiles($this->account_id, $propertyID);
        	} 
        	catch(\Exception $e) {
                return 'There was an Analytics API service error ' . $e->getCode() . ':' . $e->getMessage();
            }
        }

        public function get($propertyID, $profileID) {
        	if ( !$propertyID ) return 'Please Add Property ID';
        	if ( !$profileID ) return 'Please Add Property ID';

        	try {
            	return $this->analytic->management_profiles->get($this->account_id, $propertyID, $profileID);
        	} 
        	catch(\Exception $e) {
                return 'There was an Analytics API service error ' . $e->getCode() . ':' . $e->getMessage();
            }
        }

        public function delete($propertyID, $profileID) {
        	if ( !$propertyID ) return 'Please Add Property ID';
        	if ( !$profileID ) return 'Please Add Property ID';

        	try {
            	return $this->analytic->management_profiles->delete($this->account_id, $propertyID, $profileID);
        	} 
        	catch(\Exception $e) {
                return 'There was an Analytics API service error ' . $e->getCode() . ':' . $e->getMessage();
            }
        }

        public function insert($propertyID, $profileName) {
        	if ( !$profileName ) return 'Please Add Profile Name';
        	
        	try {

				$this->profile->setName($profileName);
	
            	return $this->analytic->management_profiles->insert($this->account_id, $propertyID, $this->profile);
        	} 
        	catch(\Exception $e) {
                return 'There was an Analytics API service error ' . $e->getCode() . ':' . $e->getMessage();
            }
        }

        public function update($propertyID, $profileID, $profileName) {
        	if ( !$profileName ) return 'Please Add Profile Name';
        	
        	$defaults = $this->get($propertyID, $profileID);

        	try {

				$this->profile->setName($profileName);
				$this->profile->setCurrency($defaults->currency);
				$this->profile->setTimezone($defaults->timezone);
				$this->profile->setWebsiteUrl(get_bloginfo('url'));
				$this->profile->setECommerceTracking(true);
				$this->profile->setEnhancedECommerceTracking(True);

            	return $this->analytic->management_profiles->update($this->account_id, $propertyID, $profileID, $this->profile);
        	} 
        	catch(\Exception $e) {
                return 'There was an Analytics API service error ' . $e->getCode() . ':' . $e->getMessage();
            }
        }
    }
}