<?php 

if (!class_exists('wb_ga_property'))
{
    class wb_ga_property
    {  
    	private $analytic;
    	private $account_id;
    	private $property;

    	function __construct($anayltic = '', $account_id = '')
        {	
        	$this->analytic = $anayltic;
        	$this->account_id = $account_id;
        	$this->property = new Google_Service_Analytics_Webproperty();
        }

        public function get($propertyID) {
        	if ( !$propertyID ) return 'Please Add Property ID';

        	try {
            	return $this->analytic->management_webproperties->get($this->account_id, $propertyID);
        	} 
        	catch(\Exception $e) {
                return 'There was an Analytics API service error ' . $e->getCode() . ':' . $e->getMessage();
            }
        }

        public function list() {
        	try {
            	return $properties = $this->analytic->management_webproperties->listManagementWebproperties($this->account_id);
        	} 
        	catch(\Exception $e) {
                return 'There was an Analytics API service error ' . $e->getCode() . ':' . $e->getMessage();
            }
        }

        public function insert($propertyName) {
        	if ( !$propertyName ) return 'Please Add Property Name';

        	try {
            	$this->property->setName($propertyName);
        	return $this->analytic->management_webproperties->insert($this->account_id, $this->property);
        	} 
        	catch(\Exception $e) {
                return 'There was an Analytics API service error ' . $e->getCode() . ':' . $e->getMessage();
            }
        }

        public function update($propertyID, $propertyName) {
        	if ( !$propertyName ) return 'Please Add Property Name';
        	if ( !$propertyID ) return 'Please Add Property ID';

        	try {
            	$this->property->setName($propertyName);
        		return $this->analytic->management_webproperties->update($this->account_id, $propertyID, $this->property);
        	} 
        	catch(\Exception $e) {
                return 'There was an Analytics API service error ' . $e->getCode() . ':' . $e->getMessage();
            }
        }
    }
}