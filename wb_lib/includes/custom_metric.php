<?php 

if (!class_exists('wb_ga_custom_metrics'))
{
    class wb_ga_custom_metrics
    {  
    	private $analytic;
    	private $account_id;
    	private $metric;

    	function __construct($anayltic = '', $account_id = '')
        {	
        	$this->analytic = $anayltic;
        	$this->account_id = $account_id;
        	$this->metric = new Google_Service_Analytics_CustomMetric();
        }

        public function list($propertyID) {
        	if ( !$propertyID ) return 'Please Add Property ID';

        	try {
            	return $this->analytic->management_customMetrics->listManagementCustomMetrics($this->account_id, $propertyID);
        	} 
        	catch(\Exception $e) {
                return 'There was an Analytics API service error ' . $e->getCode() . ':' . $e->getMessage();
            }
        }

        public function get($propertyID, $metricID) {
        	if ( !$propertyID ) return 'Please Add Property ID';
        	if ( !$metricID ) return 'Please Add Metric ID';

        	try {
            	return $this->analytic->management_customMetrics->get($this->account_id, $propertyID, $metricID);
        	} 
        	catch(\Exception $e) {
                return 'There was an Analytics API service error ' . $e->getCode() . ':' . $e->getMessage();
            }
        }

        public function insert($propertyID, $name, $type, $scope, $state = TRUE) {
        	if ( !$propertyID ) return 'Please Add Property ID';
        	if ( !$name ) return 'Please Add Metric Name';
        	if ( !$type ) return 'Please Add Metric Type';
        	if ( !$scope ) return 'Please Add Metric Scope';

        	try {

				$this->metric->setName($name);
				$this->metric->setType($type);
				$this->metric->setScope($scope);
				$this->metric->setActive($state);

            	return $this->analytic->management_customMetrics->insert($this->account_id, $propertyID, $this->metric);
        	} 
        	catch(\Exception $e) {
                return 'There was an Analytics API service error ' . $e->getCode() . ':' . $e->getMessage();
            }
        }

        public function update($propertyID, $metricID , $name, $type, $scope, $state = TRUE) {
        	if ( !$metricID) return 'Please Add Metric ID';
        	if ( !$propertyID ) return 'Please Add Property ID';
        	if ( !$name ) return 'Please Add Metric Name';
        	if ( !$type ) return 'Please Add Metric Type';
        	if ( !$scope ) return 'Please Add Metric Scope';

        	try {

				$this->metric->setName($name);
				$this->metric->setType($type);
				$this->metric->setScope($scope);
				$this->metric->setActive($state);

            	return $this->analytic->management_customMetrics->update($this->account_id, $propertyID, $metricID, $this->metric);
        	} 
        	catch(\Exception $e) {
                return 'There was an Analytics API service error ' . $e->getCode() . ':' . $e->getMessage();
            }
        }
    }
}